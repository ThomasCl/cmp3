<!--<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Thomas Claeys</title>

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/drunken-parrot.css">
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!--     NAVIGATION     -->
<nav id="mainNav" class="navbar navbar-inverse palette-the-black-flag">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php print $base_path ?>">

				<img src="<?php print $logo; ?>" alt="logo" class="logo">

				<?php print variable_get('site_name'); ?>

			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

			<ul class="nav navbar-nav navbar-right">
				<li class="searchToggle"><a href="#"><i class="glyphicon glyphicon-search"></i></a></li>
			</ul>

			<?php

			$menu = menu_navigation_links('main-menu');
			//print theme('links__menu_main_page', array('links'=> $menu));
			print theme('links', array(
				'links' => $menu,
				'attributes' => array(
					'class' => array('nav navbar-nav navbar-right'),
				)
			));

			?>

		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid -->
</nav>
<!--    Search module   -->
<div class="alert searchBox alert-warning alert-dismissable">
	<button type="button" class="close closeSearch fa-times fa"></button>

	<?php

	$block = module_invoke('search', 'block_view', 'search');
	print render($block);

	?>
</div>
<!--    End search module   -->
<!--    END NAVIGATION    -->

<!--     MAIN CONTENT     -->
<div class="container">

	<!--    Show node id if user is logged in-->
	<?php
	global $user;
	if (is_array($user->roles) && in_array('administrator', $user->roles)) { ?>
		<a href="#">Node id <span class="badge"><?php print $node->nid; ?></span></a>

		<?php
	}
	?>

	<?php if(drupal_is_front_page()){ ?>

		<h2>

			<?php print render($site_slogan); ?>

		</h2>

		<?php print render ($page['content']); ?>

		<?php print $feed_icons; ?>

		<?php
	}
	else { ?>


		<div class="col-md-6">
		<?php

		dpm($content);
		dsm($content);

		print render ($page['content']);
		print $feed_icons;

		?>
		</div>
		<div class="col-md-6">
			<img src="<?php print base_path() . path_to_theme() . '/' . 'images/team-2.jpg'?>" alt="" class="img-responsive">
		</div>
	<?php }
	?>






</div>

<!--   END MAIN CONTENT    -->

<!--   FOOTER     -->
<div class="container text-center">
	<hr />
	<div class="row">
		<div class="col-lg-12">

			<?php print render($page['footer']) ?>

			<?php  print theme('links', array(
				'links' => $page['footer'],
				'attributes' => array(
					'class' => array('nav navbar-nav navbar-right'),
				)
			)); ?>

		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-lg-12">
			<ul class="nav nav-pills nav-justified">
				<li><a href="/">© <span id="year"></span> Thomas Claeys</a></li>
				<li><a href="#">Terms of Service</a></li>
				<li><a href="#">CMS By Drupal</a></li>
			</ul>
		</div>
	</div>
</div>

<!--    END FOOTER    -->
<!--<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/parallax.min.js"></script>
<script src="js/script.js"></script>-->
</body>
</html>