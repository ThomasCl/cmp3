$( document ).ready(function() {

        //$("#owl-example").owlCarousel();
        //



    var currentYear = (new Date).getFullYear();

    $("#year").text(currentYear);


//Search Toggle
    $(function () {
        $(".searchToggle").click(function() {
            $(".searchBox").toggleClass('active');
        })

        $(".closeSearch").click(function () {
            $(".searchBox").toggleClass('active');
        })
    });

//    Responsive video

    // Find all YouTube videos
    var $allVideos = $("iframe[src^='//www.youtube.com']");
    var $allVideos = $("iframe[src^='//player.vimeo.com'], iframe[src^='//www.youtube.com']"),

    // The element that is fluid width
        $fluidEl = $(".portfolio-header");

// Figure out and save aspect ratio for each video
    $allVideos.each(function() {

        $(this)
            .data('aspectRatio', this.height / this.width)

            // and remove the hard coded width/height
            .removeAttr('height')
            .removeAttr('width');

    });

// When the window is resized
    $(window).resize(function() {

        var newWidth = $fluidEl.width();

        // Resize all videos according to their own aspect ratio
        $allVideos.each(function() {

            var $el = $(this);
            $el
                .width(newWidth)
                .height(newWidth * $el.data('aspectRatio'));

        });

// Kick off one resize to fix all videos on page load
    }).resize();

});



