<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!--     NAVIGATION     -->
<nav id="mainNav" class="navbar navbar-inverse palette-the-black-flag">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php print $base_path ?>">

                <img src="<?php print $logo; ?>" alt="logo" class="logo">

                <?php print variable_get('site_name'); ?>

            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar-right">
                <li class="searchToggle"><a href="#"><i class="glyphicon glyphicon-search"></i></a></li>
            </ul>

            <?php

            $menu = menu_navigation_links('main-menu');
            //print theme('links__menu_main_page', array('links'=> $menu));
            print theme('links', array(
                'links' => $menu,
                'attributes' => array(
                    'class' => array('nav navbar-nav navbar-right'),
                )
            ));

            ?>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<!--    Search module   -->
<div class="alert searchBox alert-warning alert-dismissable">
    <button type="button" class="close closeSearch fa-times fa"></button>

    <?php

    $block = module_invoke('search', 'block_view', 'search');
    print render($block);

    ?>
</div>
<!--    End search module   -->
<!--    END NAVIGATION    -->

<!--     MAIN CONTENT     -->
<div class="container-fluid">

    <div class="row">

        <?php if(drupal_is_front_page()){ ?>

            <h2 class="slogan">

                <?php print render($site_slogan); ?>

            </h2>

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <?php print render($page['homecarousel']); ?>
                </div>
            </div>


            <div class="row homeSection">
                <div class="col-md-8 col-md-offset-2">

                    <?php

                    // node to show teaser from
                    $nid = 1;

                    $view_mode = 'teaser';
                    $node = node_load($nid);
                    print render(node_view($node, $view_mode));


                    ?>

                </div>
            </div>

            <div class="row palette palette-overboard homeSection">
                <div class="col-md-8 col-md-offset-2">

                    <?php

                    // node to show teaser from
                    $nid = 23;

                    $view_mode = 'teaser';
                    $node = node_load($nid);
                    print render(node_view($node, $view_mode));


                    ?>

                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <?php print render($page['recent']); ?>
                </div>
            </div>

            <div class="row palette palette-overboard homeSection">
                <div class="col-md-8 col-md-offset-2">
                    <?php print render($page['recentblogposts']); ?>
                </div>
            </div>

            <div class="row homeSection">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <?php print render($page['newslettersubscribe']); ?>
                </div>
            </div>


            <?php
        }

        ?>

    </div>

</div>

<!--   END MAIN CONTENT    -->

<!--   FOOTER     -->
<div class="container text-center">
    <hr />
    <div class="row">
        <div class="col-lg-12">

            <?php print render($page['footer']) ?>

            <?php  print theme('links', array(
                'links' => $page['footer'],
                'attributes' => array(
                    'class' => array('nav navbar-nav navbar-right'),
                )
            )); ?>

        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-12">
            <ul class="nav nav-pills nav-justified">
                <li>© <span id="year"></span> Thomas Claeys</li>
            </ul>
        </div>
    </div>
</div>

<!--    END FOOTER    -->
<!--<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/parallax.min.js"></script>
<script src="js/script.js"></script>-->
</body>
</html>